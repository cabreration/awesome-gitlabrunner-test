// modulos externos
const cors = require('cors');
const express = require('express');
const app = express();

// configuraciones servidor
app.use(express.json());
app.use(cors());

app.get('/test', (req, res) => {
    res.send({
        state: "Hello, World!"
    })
});

app.listen(80, () => console.log("Microservicio de Cliente en puerto 80"));